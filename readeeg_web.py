# -*- coding: utf-8 -*-
"""
==============================================================================
    BrainUp
==============================================================================

    Source Name   : readeeg_web.py
    Author        : Fangkun Zhu, Jinhui Qian
    Date          : 10/12/2021

==============================================================================
"""
import numpy as np
import scipy.io

keypath = r"E:\naolu\sanity check\2\1639038542-jinming_web.txt" # 文件路径
rfs = 250
channel = 5

data = np.loadtxt(keypath, dtype=str, delimiter=",")
time = data.shape[0]
length = data.shape[1] # 2000 1秒的数据reshape，8*250
floatData = np.zeros((time, length))
for ii in range(time):
    for l in range(length):
        strdata = data[ii, l]
        strdata = strdata.translate(str.maketrans('', '', ']'))
        strdata = strdata.translate(str.maketrans('', '', '['))
        strdata = strdata.translate(str.maketrans('', '', ']]'))
        strdata = strdata.translate(str.maketrans('', '', '[['))
        floatData[ii, l] = float(strdata)
# # 转换为浮点数后reshape矩阵,
# data_Float = floatData.reshape(time, 5, rfs)
# # data2检查无误
# data2 = np.transpose(data_Float, [1, 2, 0])
chan = []
for i in range(channel):
    temp = floatData[:, i*rfs:(i+1)*rfs]
    chan.append(temp.reshape((1,-1)))

EEG_web = np.squeeze(np.array(chan))
scipy.io.savemat('websocket_modified.mat', {'mydata': EEG_web})

    
    