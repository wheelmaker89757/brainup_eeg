"""
==============================================================================
    BrainUp
==============================================================================

    Source Name   : unitydisplay.py
    Author        : Jinhui Qian
    Date          : 13/12/2021

==============================================================================
"""
import numpy as np
from scipy.signal import butter, lfilter, savgol_filter, filtfilt, welch
import pdb

class display:
    
    def __init__(self, sampling_freq):
        self.sampling_freq = sampling_freq
        
    def normalize(self, data):
        # res = []
        # for item in data:
        temp1 = self.utility_freq_removal(data, self.sampling_freq)
        # temp2 = self.bandpass_filtering(temp1, sampling_freq) 
        # pdb.set_trace()
        temp2 = self.butter_lowpass_filter(temp1, self.sampling_freq, 35)    
        res = self.butter_highpass_filter(temp2, self.sampling_freq, 3)
        # res = self.avg_reference(temp2)
            # res.append(temp3)
        return res
    
    def bandpass_filtering(self, data, Sampling_freq, filter_name = "Butter", low_cut = 0.5, high_cut = 50):
        if (filter_name == "SG"):
            data = savgol_filter(data, 5, 3)
        elif (filter_name == "Butter"):
            b, a = self.butter_bandpass(low_cut, high_cut, Sampling_freq)
            data = lfilter(b, a, data)
            return data
    def butter_bandpass(self, low_cut, high_cut, fs, order=3):
        nyq = 0.5 * fs
        low = low_cut / nyq
        high = high_cut / nyq
        b, a = butter(order, [low, high], btype='band')
        return b, a
    def avg_reference(self, data):
        avg_channel = np.mean((data), axis=0)
        for channel in range(data.shape[0]):
            data[channel,:] -= avg_channel
            return data
        
    def utility_freq_removal(self, data, Sampling_freq):
            # utility frequency removal
        b0, a0 = butter(4, [45, 55], 'bandstop', fs=Sampling_freq)
        data = filtfilt(b0, a0, data)
        return data
    def butter_lowpass_filter(self, data, fs, cutoff, order=5):
        nyq = 0.5 * fs
        normal_cutoff = cutoff / nyq
        b, a = butter(order, normal_cutoff, btype='low', analog=False)
        y = lfilter(b, a, data)
        return y
    def butter_highpass_filter(self, data, fs, cutoff, order=5):
        nyq = 0.5 * fs
        high = cutoff / nyq
        b, a = butter(order, high, btype='high', analog=False)
        y = lfilter(b, a, data)
        return y
    
        
    
    

