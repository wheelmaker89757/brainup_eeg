"""
==============================================================================
    BrainUp
==============================================================================

    Source Name   : EEGQE.py
    Author        : Jinhui Qian
    Date          : 14/12/2021

==============================================================================
"""
import numpy as np
import pdb

class Evaluation:
    
    def __init__(self, sampling_freq = 250, thresh1 = 0.48, thresh2 = 8.77):
        self.sampling_freq = sampling_freq
        self.thresh1 = thresh1
        self.thresh2 = thresh2
        
    def evaluation(self, data):
        var,max_val = 0,0
        for i in range(data.shape[0]):
            curr = data[i]
            var = max(var, np.var(curr))
            max_val = max(max_val, max(curr**2))
        if var<self.thresh1 and max_val<self.thresh2:
            return True
        else:
            return False
            
            