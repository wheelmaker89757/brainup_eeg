% Note:
%     1. 输入：路径名+文件名 （str）
%     2. 文件由雅典娜平台所得，采样率250Hz, 5channel，35列
%     3. 输出：5channel * nsamples
function RAWEEG = readeegdata_athena(filename) 
% load data and data partitioning
raw = load(filename);
rows = 5 * size(raw,1);
data = raw(:,3:27);
d1 = data(:,1:5);
d2 = data(:,6:10);
d3 = data(:,11:15);
d4 = data(:,16:20);
d5 = data(:,21:25);

% EEG recomposition
res = zeros(rows,5);
res(1:5:end-4,:) = d1;
res(2:5:end-3,:) = d2;
res(3:5:end-2,:) = d3;
res(4:5:end-1,:) = d4;
res(5:5:end,:) = d5;

% EEG cleansing and store in .mat file
for i = 1:rows
    if (any(res(i,:)))
        break;
    end
end
RAWEEG = res(i:end,:)';
end